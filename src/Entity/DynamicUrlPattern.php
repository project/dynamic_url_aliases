<?php

namespace Drupal\dynamic_url_aliases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\dynamic_url_aliases\DynamicUrlPatternInterface;
use InvalidArgumentException;

/**
 * Defines the Dynamic url pattern entity.
 *
 * @ConfigEntityType(
 *   id = "dynamic_url_pattern",
 *   label = @Translation("Dynamic url pattern"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\dynamic_url_aliases\DynamicUrlPatternListBuilder",
 *     "form" = {
 *       "add" = "Drupal\dynamic_url_aliases\Form\DynamicUrlPatternForm",
 *       "edit" = "Drupal\dynamic_url_aliases\Form\DynamicUrlPatternForm",
 *       "delete" = "Drupal\dynamic_url_aliases\Form\DynamicUrlPatternDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\dynamic_url_aliases\DynamicUrlPatternHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "dynamic_url_pattern",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/dynamic_url_pattern/{dynamic_url_pattern}",
 *     "add-form" = "/admin/structure/dynamic_url_pattern/add",
 *     "edit-form" = "/admin/structure/dynamic_url_pattern/{dynamic_url_pattern}/edit",
 *     "delete-form" = "/admin/structure/dynamic_url_pattern/{dynamic_url_pattern}/delete",
 *     "collection" = "/admin/structure/dynamic_url_pattern"
 *   }
 * )
 */
class DynamicUrlPattern extends ConfigEntityBase implements DynamicUrlPatternInterface {

  /**
   * The Dynamic url pattern ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Dynamic url pattern label.
   *
   * @var string
   */
  protected $label;

  /**
   * The pattern to match urls to be aliased.
   *
   * @var string
   */
  protected $pattern;

  /**
   * The dynamic alias.
   *
   * @var string
   */
  protected $to;

  /**
   * Gets the pattern and converts it to regex.
   *
   * @return string
   *   The regex.
   */
  protected function getRegexPattern() {
    $pattern = $this->getPattern();

    // Convert anything that has a '%' before it to '\w+'
    $pattern = preg_replace(self::CONTEXT_FRAGMENT_PATTERN_REPLACE, "(\w+)", $pattern);

    // Escape any slash.
    $pattern = str_replace('/', '\/', $pattern);

    return '/' . $pattern . '/';
  }

  private static function throwInvalidUrl($url, $pattern) {
    throw new InvalidArgumentException("The url '$url' doesn't match the pattern '$pattern'");
  }

  /**
   * {@inheritDoc}
   */
  public function getPattern() {
    return $this->pattern;
  }

  /**
   * {@inheritDoc}
   */
  public function setPattern($pattern) {
    $this->pattern = $pattern;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getToUrl() {
    return $this->to;
  }

  /**
   * {@inheritDoc}
   */
  public function setToUrl($url) {
    $this->to = $url;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function aliasUrl($url) {
    if (!$this->isApplicable($url)) {
      self::throwInvalidUrl($url, $this->getPattern());
    }

    $entities = $this->loadEntities($url);
    return \Drupal::token()->replace($this->getToUrl(), $entities);
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicable($url) {
    $pattern = $this->getRegexPattern();
    // Convert the pattern to regex to test it against the $url
    return (bool) preg_match($pattern, $url);
  }

  /**
   * {@inheritDoc}
   */
  public function loadEntities($url) {
    $pattern = $this->getRegexPattern();

    // Get the IDs using the pattern.
    $matches = [];
    preg_match_all($pattern, $url, $matches);

    // Get the entity type IDs.
    $entity_type_ids = $this->getEntityTypesFromPattern();

    // Test if applicable.
    if (count($matches) - 1 != count($entity_type_ids)) {
      self::throwInvalidUrl($url, $pattern);
    }

    // Begin loading the entities.
    $data = [];
    $current = 0;
    foreach ($entity_type_ids as $entity_type_id) {
      $data[$entity_type_id] = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($matches[++$current][0]);
    }

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityTypesFromPattern() {
    $matches = [];
    preg_match_all(self::CONTEXT_FRAGMENT_PATTERN_SEARCH, $this->getPattern(), $matches);
    return $matches[0];
  }

}
