<?php

namespace Drupal\dynamic_url_aliases;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Dynamic url pattern entities.
 */
interface DynamicUrlPatternInterface extends ConfigEntityInterface {

  /**
   * The context fragment pattern to get the fragments of a pattern.
   *
   */
  const CONTEXT_FRAGMENT_PATTERN_SEARCH = "/(?<=%)\w+/";

  /**
   * The context fragment pattern to replace the fragments of a pattern.
   *
   */
  const CONTEXT_FRAGMENT_PATTERN_REPLACE = "/%\w+/";

  /**
   * Aliases a url.
   *
   * @param string $url
   *   The url.
   *
   * @return string
   *   The new aliased url
   */
  public function aliasUrl($url);

  /**
   * Check if the url can be aliased.
   *
   * @param string $url
   *   The url to check.
   *
   * @return boolean
   */
  public function isApplicable($url);

  /**
   * Gets the pattern.
   *
   * @return string
   *   The pattern.
   */
  public function getPattern();

  /**
   * Sets the pattern
   *
   * @param string $pattern
   *   The pattern.
   *
   * @return \Drupal\dynamic_url_aliases\DynamicUrlPatternInterface
   */
  public function setPattern($pattern);

  /**
   * Gets the 'to' url with tokens.
   *
   * @return string
   *   The url with tokens.
   */
  public function getToUrl();

  /**
   * Sets the 'to' url.
   *
   * @param string $url
   *   The url with tokens.
   *
   * @return \Drupal\dynamic_url_aliases\DynamicUrlPatternInterface
   */
  public function setToUrl($url);

  /**
   * Gets the entities from a url.
   *
   * @param string $url
   *   The url to check.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entitites.
   */
  public function loadEntities($url);

  /**
   * Gets the entity type IDs from the pattern.
   *
   * @return array
   *   The contexts containing entity_type IDs
   */
  public function getEntityTypesFromPattern();

}
