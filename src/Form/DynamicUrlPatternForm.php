<?php

namespace Drupal\dynamic_url_aliases\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DynamicUrlPatternForm.
 */
class DynamicUrlPatternForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $dynamic_url_pattern = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $dynamic_url_pattern->label(),
      '#description' => $this->t("Label for the Dynamic url pattern."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $dynamic_url_pattern->id(),
      '#machine_name' => [
        'exists' => '\Drupal\dynamic_url_aliases\Entity\DynamicUrlPattern::load',
      ],
      '#disabled' => !$dynamic_url_pattern->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $dynamic_url_pattern = $this->entity;
    $status = $dynamic_url_pattern->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Dynamic url pattern.', [
          '%label' => $dynamic_url_pattern->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Dynamic url pattern.', [
          '%label' => $dynamic_url_pattern->label(),
        ]));
    }
    $form_state->setRedirectUrl($dynamic_url_pattern->toUrl('collection'));
  }

}
