<?php

namespace Drupal\Tests\dynamic_url_aliases\Unit;

class StorageMock {

  /**
   * The entity type id.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Dummy data.
   *
   * @var array
   */
  protected $data;

  public function __construct($entity_type_id, array $data) {
    $this->entityTypeId = $entity_type_id;
    $this->data = $data;
  }

  public function load($id) {
    $storage = $this->data[$this->entityTypeId];
    if (isset($storage[$id])) {
      $entity = $storage[$id];
    } else {
      $entity = NULL;
    }
    return $entity;
  }

}