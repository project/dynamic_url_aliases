<?php

namespace Drupal\Tests\dynamic_url_aliases\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\dynamic_url_aliases\Entity\DynamicUrlPattern;
use Drupal\node\NodeInterface;
use Drupal\Tests\dynamic_url_aliases\Unit\StorageMock;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use InvalidArgumentException;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group dynamic_url_aliases
 */
class DynamicUrlPatternTest extends UnitTestCase {

  /**
   * A mock object of DynamicUrlPattern entity
   *
   * @see \Drupal\dynamic_url_aliases\Entity\DynamicUrlPattern
   *
   * @var \Drupal\dynamic_url_aliases\DynamicUrlPatternInterface
   */
  protected $dynamicUrlPattern;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $dynamicUrlPatternMock = $this->getMockBuilder(\Drupal\dynamic_url_aliases\Entity\DynamicUrlPattern::class)
      ->disableOriginalConstructor()
      ->setMethods()
      ->getMock();

    $this->dynamicUrlPattern = $dynamicUrlPatternMock;

    // Mock entity entity_type_id manager
    $container = new ContainerBuilder();

    $entity_type_manager_mock = $this->getMockBuilder(\Drupal\Core\Entity\EntityTypeManager::class)->disableOriginalConstructor()
      ->setMethodsExcept()->getMock();

    $entity_type_manager_mock->method('getStorage')->willReturnCallback([$this, 'mockStorage']);

    $container->set('entity_type.manager', $entity_type_manager_mock);

    // Mock token
    $token_mock = $this->getMockBuilder(Token::class)->disableOriginalConstructor()
      ->setMethodsExcept()->getMock();
    $token_mock->method('replace')->willReturnCallback([$this, 'mockTokenReplace']);
    $container->set('token', $token_mock);

    \Drupal::setContainer($container);
  }

  /**
   * Tests \Drupal\dynamic_url_aliases\DynamicUrlPattern::getEntityTypesFromPattern() method.
   */
  public function testgetEntityTypesFromPattern() {
    $this->dynamicUrlPattern->setPattern('/test-page/%node/user/%user');
    $results = $this->dynamicUrlPattern->getEntityTypesFromPattern();

    $this->assertCount(2, $results);
    $this->assertArrayEquals([
      0 => 'node',
      1 => 'user',
    ], $results);
  }

  /**
   * Tests \Drupal\dynamic_url_aliases\DynamicUrlPattern::isApplicable() method.
   */
  public function testIsApplicable() {
    $mock = $this->dynamicUrlPattern;

    // Set the pattern.
    $mock->setPattern('/user/%user');

    // Test
    $this->assertTrue($mock->isApplicable('/user/10'));
    $this->assertTrue($mock->isApplicable('/user/99999'));
    $this->assertTrue($mock->isApplicable('/user/john'));
    $this->assertFalse($mock->isApplicable('/node/10'));
    $this->assertFalse($mock->isApplicable('/node/beta'));
    $this->assertFalse($mock->isApplicable('/user'));
  }

  /**
   * Tests \Drupal\dynamic_url_aliases\DynamicUrlPattern::aliasUrl() method
   */
  public function testAliasUrl() {
    $mock = $this->dynamicUrlPattern;
    $mock->setPattern('/user/%user');
    $mock->setToUrl('/my/aliased/url/[user:id]');

    $aliased_url = $mock->aliasUrl('/user/11');
    $this->assertEquals($aliased_url, '/my/aliased/url/11');

    // Expect an exception if the url doesn't match the the pattern.
    $this->expectException(InvalidArgumentException::class);
    $mock->aliasUrl('/node/10');
    $mock->aliasUrl('/user/10/edit');
    // Expect an exception if the url is empty.
    $mock->aliasUrl("");
  }

  /**
   * Tests \Drupal\dynamic_url_aliases\DynamicUrlPattern::loadEntities() method
   */
  public function testLoadEntities() {
    $mock = $this->dynamicUrlPattern;
    $mock->setPattern('/user/%user');

    $result = $mock->loadEntities('/user/11');
    $this->assertTrue($result['user'] instanceof UserInterface);

    $mock->setPattern('/content/%user/view/%node');

    $result = $mock->loadEntities('/content/11/view/22');
    $this->assertTrue($result['user'] instanceof UserInterface);
    $this->assertTrue($result['node'] instanceof NodeInterface);

    $result = $mock->loadEntities('/content/11/view/1000');
    $this->assertTrue($result['user'] instanceof UserInterface);
    $this->assertEmpty($result['node']);
  }

  /**
   * Returns a mock storage.
   *
   * @return \Drupal\Tests\dynamic_url_aliases\StorageMock
   *   The testing storage.
   */
  public function mockStorage($entity_type_id) {
    // Create a user mock.
    $user_mock = $this->getMockBuilder(\Drupal\user\Entity\User::class)->disableOriginalConstructor()
      ->setMethodsExcept()->getMock();
    $user_mock->method('id')->willReturn(11);
    // Create a node mock.
    $node_mock = $this->getMockBuilder(\Drupal\node\Entity\Node::class)->disableOriginalConstructor()
      ->setMethodsExcept()->getMock();
    $node_mock->method('id')->willReturn(22);

    $data = [
      'user' => [
        11 => $user_mock,
      ],
      'node' => [
        22 => $node_mock,
      ],
    ];
    return new StorageMock($entity_type_id, $data);
  }

  /**
   * Mocks Token::replace();
   */
  public function mockTokenReplace($replacement, array $data) {
    $user = $data['user'];
    return str_replace('[user:id]', $user->id(), $replacement);
  }

}
